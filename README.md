# easyWave

easyWave is an application that is used to simulate tsunami generation and propagation in the context of early warning. It makes use of GPU acceleration to speed up the calculations. easyWave is licensed under the [EUPL](https://joinup.ec.europa.eu/page/eupl-text-11-12) (v1.1 or later), complemented with the following provision: For the scientific transparency and verification of results obtained and communicated to the public after using a modified version of the work, You (as the recipient of the source code and author of this modified version, used to produce the published results in scientific communications) commit to make this modified source code available in a repository that is easily and freely accessible for a duration of five years after the communication of the obtained results.


# Download

The latest CPU version supports multi-threading on shared memory systems using OpenMP.

 * [easywave_r34_omp_i386.tar.gz](bin/r34/easywave_r34_omp_i386.tar.gz)
 * [easywave_r34_omp_x64.tar.gz](bin/r34/easywave_r34_omp_x64.tar.gz)
 * [easywave_r34_src.tar.gz](bin/r34/easywave_r34_src.tar.gz)

You can download one of the following packages to use the single-threaded version or a pre-compiled GPU binary instead.

 * [easywave_r17_gpu_i386.tar.gz](bin/r17/easywave_r17_gpu_i386.tar.gz)
 * [easywave_r17_gpu_x64.tar.gz](bin/r17/easywave_r17_gpu_x64.tar.gz)
 * [easywave_r17_i386.tar.gz](bin/r17/easywave_r17_i386.tar.gz)
 * [easywave_r17_x64.tar.gz](bin/r17/easywave_r17_x64.tar.gz)
 * [easywave_r17_src.tar.gz](bin/r17/easywave_r17_src.tar.gz)


# Getting started

The following sections will explain how to install and use easyWave. The guide is written for Unix-based systems and assumes that a bash shell is used. Other shells may require little modifications.


## Installation

You can install easyWave in three different ways:

 1. Download a pre-built binary package appropriate for your system.
 2. Download a source file archive and compile on your own.
 3. Check out a development branch via git

In the following all the possibilities are described.


### 1. Using binaries

 1. [Download](#download) a suitable package according to your platform into some folder DIR.
 2. Extract the package with an archive manager. Therefore open a terminal, browse to the download directory and run the following command:
```shell
tar -xzf <package>.tar.gz
```

 3. You can now use easyWave by specifying the absolute path /DIR/easywave/bin/easywave. However you can also add the bin-folder of easyWave to your `$PATH` variable like that:
```shell
export PATH=$PATH:$PWD/easywave/bin
```
**Note**: To make the change permanent add the above command to your `.bashrc`, but replace `$PWD` with the working directory.  Afterwards you can run easyWave simply by its program name: `easywave`

The installation is complete. You can now continue with the [Examples](#examples) section.


### 2. From a source archive

 1. [Download](#download) the source archive to an arbitrary directory in your file system.
 2. Open a terminal, browse to the selected directory and unpack the downloaded archive (This will create a directory named `easywave` that contains all sources that are necessary to build the executables.):
```shell
tar -xzf <source>.tar.gz
```

 3. The `configure` script will search your system for compatible tools to compile the source code. It will also set some default options for the installation process. You can customize these options on the command line. To get a list of options type `./configure --help`. If the above steps have been successfully completed all binary files have been created.  Change to this directory and build the sources: 
```shell
cd easywave
./configure
make
```

 4. You could now install easyWave to the specified installation directory (default: `/usr/local/easywave`) by typing:
```shell
make install
```
**Note**: If the installation directory is not writable by the current user, you have to run the above command as root. 
After that easyWave was successfully installed and is ready to use. We recommend to add the installation's bin-directory to the `$PATH` variable to use easyWave from any location.


### 3. From the git repository

You can download the latest development state or any custom branch of easyWave directly from the `git` repository. For this, you need `git` installed on your system.

 1. Open a terminal and browse to a directory where you want to place a copy of easyWave.
 2. Check out the trunk:
```shell
git clone https://gitext.gfz-potsdam.de/geoperil/easyWave.git
```

 3. Run the following instructions:
```shell
cd easyWave/code
./bootstrap
./configure
make
make install
```

For details see [Section 2](#2-from-a-source-archive)

The git clone will also include some contents that is not part of the distributed tar.gz archive. Especially the tests/ directory can be useful to ensure program functionality. For details see section [Test](#tests).


## Examples

To get familiar with easyWave we provide some example input files that can be used to run certain simulations. In the following we will give the instructions for getting started with easyWave.

 1. Create a directory on your file system where the example files should be placed.
 2. Download the [archive](data/examples.tar.gz) that contains the examples into the newly created directory.
 3. Open a terminal, browse to the selected directory and unpack the downloaded archive:
This will create a directory named `examples` that contains all example files. 
**Note**: In the following we will assume that you have added easyWave to your `$PATH` and thus it can simply be called by its name. Otherwise you have to add the full path when calling easywave. 
```shell
tar -xzf examples.tar.gz
```

 4. Now you can run your first simulation with the following command:
```shell
easywave -grid examples/e2Asean.grd -source examples/BengkuluSept2007.flt -time 120
```
easyWave will simulate the tsunami generation and propagation from the given source according to the underlying grid file for a duration of 120 minutes. This can take a moment. However the progress is shown at run time. If the calculation is finished the following files have been created:
  * `eWave.2D.sshmax`: Contains the maximum wave heights.
  * `eWave.2D.XXXXX.ssh`: Contains the wave heights after XXXXX seconds of simulation.
  * `eWave.2D.time`: Contains the estimated arrival times.
  * `easywave.log`: Contains log messages.

To visualize the results you can use the scripts supplied with the easyWave installation. To show the maximum wave heights run the following command:
```shell
sshmax2png.sh -grd examples/e2Asean.grd
```

This will create a file named `eWave.2D.png`. You can open this file with your default image viewer.
You can also visualize the wave propagation at a particular time. Simply run:
```shell
ssh2png.sh -grd examples/e2Asean.grd 03600
```

This again generates an image file that in this case is named `eWave.2D.03600.png`. It contains the wave propagation after 3600 seconds (= 1 hour). 
Furthermore you can display the given points of interests if easyWave was started with a corresponding POI file:
```shell
poi2png.sh -grd examples/e2Asean.grd examples/poiIndonesia.poi
```


## Tests

The `tests/` directory contains some python scripts that can be used to compare different easyWave versions with each other. You should always perform tests if you have made changes. This makes sure that your version still yields satisfying results.

To invoke a test run you mainly need to call `tests.py`. To obtain the test cases two configuration files were used. You can specify them with the arguments `--instances` (`-i`) and `--scenarios` (`-s`). The file given by `--instances` should include paths to easyWave instances that should be compared to each other. Each line should contain exactly two instances separated by a semicolon. For example:

```shell
@../src/easywave; @../src/easywave -gpu
```

This leads to a comparison of the CPU and GPU version of an easyWave instance located in the `src/` directory. The @ sign should be included everywhere you want to specify a path relative to the `tests/` directory. It will be expanded automatically.

The file given by `--scenarios` should contain all parameters that should be added to the both compared easyWave instances, thus creating a scenario. You can set multiple scenarios (one per line) which are processed in order. For example:

```shell
-grid @grids/e2Asean.grd -source @faults/BengkuluSept2007.flt -time 300
-grid @grids/g08Japan.grd -source @faults/uz.Tohoku11.grd -time 300
```

The `tests/` directory already includes two default configuration files (`instances.cfg` and `scenarios.cfg`) which can be used to run a large test. However you need some additional files which must be downloaded first. To do so, simply type:

```shell
download.py
```

Note: This will download some large grid-files. 
Afterwards you can run `test.py` without passing any parameter. In this case the default configuration from `instances.cfg` and `scenarios.cfg` is used.


## Additional material

The links below refer to some additional documentation about easyWave. Parts of them may be out of date, so you should always consult this page to get the latest information.

 * [easyWave_About.pdf](doc/easyWave_About.pdf) 
 * [easyWave_StartingGuide.pdf](doc/easyWave_StartingGuide.pdf)


