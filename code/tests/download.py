#!/usr/bin/env python

import sys
import os
import subprocess
import re

sdir = os.path.dirname( os.path.abspath( sys.argv[0] ) )

grids = sdir + "/grids"
faults = sdir + "/faults"
pois = sdir + "/pois"

if os.access( grids, os.R_OK ) or os.access( faults, os.R_OK ) or os.access( pois, os.R_OK ):
  print 'The directories grids/, faults/ and pois/ are already present. Abort.'
  sys.exit(1)

p = subprocess.Popen('svn info --xml', shell=True, stdout=subprocess.PIPE)
ret = p.wait()

lines = ""
for line in p.stdout.readlines():
  lines += line

matchBase = re.search( "<root>(.*)</root>", lines )
matchPath = re.search( "<wcroot-abspath>(.*)</wcroot-abspath>", lines )

if matchBase and matchPath:
  svnbase = matchBase.group(1)
  svnpath = matchPath.group(1)
  if svnbase == "http://svnext.gfz-potsdam.de/easywave":
    # data directory should be present, so just create symbolic links
    os.symlink( svnpath + "/data/grids", grids )
    os.symlink( svnpath + "/data/faults", faults )
    os.symlink( svnpath + "/data/pois", pois )
    sys.exit(0)

# directories not present and no suitable svn root was found --> download contents
link = "http://svnext.gfz-potsdam.de/easywave/data"
ret = 0
ret += subprocess.Popen('svn export ' + link + '/grids', shell=True).wait()
ret += subprocess.Popen('svn export ' + link + '/faults', shell=True).wait()
ret += subprocess.Popen('svn export ' + link + '/pois', shell=True).wait()

if ret > 0:
    print 'Error: Could not download data.'
    sys.exit(1)
    
sys.exit(0)
