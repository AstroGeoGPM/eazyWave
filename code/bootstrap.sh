#!/bin/sh

if test $# -gt 0 -a "$1" = "clean"; then

  # clean all files created by autoreconf
  rm -rf Makefile Makefile.in aclocal.m4 autom4te.cache \
	 config.h config.h.in config.log config.status  \
	 configure depcomp install-sh missing src/.deps \
	 src/Makefile src/Makefile.in
  exit 0
fi

autoreconf -i
